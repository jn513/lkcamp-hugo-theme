var teme_selector = document.getElementById("theme-select");
var teme_selector1 = document.getElementById("theme-select1");
var voltar_ao_topo = document.getElementById("top-back");

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; SameSite=Strict; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function darkMode(){
    setCookie("tema", "dark", 10000);
    teme_selector.children[0].textContent = " Claro";
    teme_selector.children[0].classList.remove("fa-moon");
    teme_selector.children[0].classList.add("fa-sun");
    teme_selector1.children[0].textContent = " Claro";
    teme_selector1.children[0].classList.remove("fa-moon");
    teme_selector1.children[0].classList.add("fa-sun");
    document.querySelector("link[rel='shortcut icon']").href = "/images/logo-white.svg";
    document.getElementById("nav-logo").setAttribute("src", "/images/logo-white.svg");
    document.documentElement.style.setProperty("--header-background", "#1c1c1e");
    document.documentElement.style.setProperty("--header-border-boton", "#eaeaea");
    document.documentElement.style.setProperty("--title-color", "#fff1ff");
    document.documentElement.style.setProperty("--code-background", "#e8e5de");
    document.documentElement.style.setProperty("--pre-background", "#202020"); //#202020
    document.documentElement.style.setProperty("--pre-color", "#d0d0d0");
    document.documentElement.style.setProperty("--blockquote-background", "#f5f5f5");
    document.documentElement.style.setProperty("--blockquote-border", "#f5f5f5");
    document.documentElement.style.setProperty("--nav-links-color", "#f0f0f0");
    document.documentElement.style.setProperty("--nav-links-hover", "#dad4d4");
    document.documentElement.style.setProperty("--page-links-color", "#dad4d4");
    document.documentElement.style.setProperty("--footer-links", "#dad4d4");
    document.documentElement.style.setProperty("--footer-links-hover", "#d0d4d4");
    document.documentElement.style.setProperty("--list-item-color", "#f5f5f5");
    document.documentElement.style.setProperty("--list-item-border", "#f0f0f0");
    document.documentElement.style.setProperty("--article-image-border", "#e6e6e6");
    document.documentElement.style.setProperty("--article-taxonomy-hr-background", "#e8e8e8");
    document.documentElement.style.setProperty("--article-hover", "#606060");
    document.documentElement.style.setProperty("--body-background", "#152238");
    document.documentElement.style.setProperty("--content-background", "#152238");
    document.documentElement.style.setProperty("--article-taxonomy-color", "#dad4d4");
}

function whiteMode(){
    setCookie("tema", "white", 10000);
    teme_selector.children[0].textContent = " Escuro";
    teme_selector.children[0].classList.remove("fa-sun");
    teme_selector.children[0].classList.add("fa-moon");
    teme_selector1.children[0].textContent = " Escuro";
    teme_selector1.children[0].classList.remove("fa-sun");
    teme_selector1.children[0].classList.add("fa-moon");
    document.querySelector("link[rel='shortcut icon']").href = "/images/logo.svg";
    document.getElementById("nav-logo").setAttribute("src", "/images/logo.svg");
    document.documentElement.style.setProperty("--header-background", "#f5f5f5");
    document.documentElement.style.setProperty("--header-border-boton", "#eaeaea");
    document.documentElement.style.setProperty("--title-color", "#222");
    document.documentElement.style.setProperty("--code-background", "#f0f0f0");
    document.documentElement.style.setProperty("--pre-background", "#202020");
    document.documentElement.style.setProperty("--pre-color", "#d0d0d0");
    document.documentElement.style.setProperty("--blockquote-background", "#f5f5f5");
    document.documentElement.style.setProperty("--blockquote-border", "#ccc");
    document.documentElement.style.setProperty("--nav-links-color", "#444");
    document.documentElement.style.setProperty("--nav-links-hover", "#111");
    document.documentElement.style.setProperty("--page-links-color", "#555");
    document.documentElement.style.setProperty("--footer-links", "#888");
    document.documentElement.style.setProperty("--footer-links-hover", "#222");
    document.documentElement.style.setProperty("--list-item-color", "#222");
    document.documentElement.style.setProperty("--list-item-border", "#ddd");
    document.documentElement.style.setProperty("--article-image-border", "#e6e6e6");
    document.documentElement.style.setProperty("--article-taxonomy-hr-background", "#e8e8e8");
    document.documentElement.style.setProperty("--article-hover", "#606060");
    document.documentElement.style.setProperty("--body-background", "#ffffff");
    document.documentElement.style.setProperty("--content-background", "#ffffff");
    document.documentElement.style.setProperty("--article-taxonomy-color", "#404040");
}


function init(){
    var teme = getCookie("tema");

    if(teme == "white"){
        whiteMode();
    } else {
        darkMode();
    }

    voltar_ao_topo.addEventListener("click", (event) => {
        window.scroll(0,0);
    })

    teme_selector1.addEventListener( "click", (event) => {
        var teme = getCookie("tema");

        if(teme == "dark"){
            whiteMode();
        } else {
            darkMode();
        }
    });

    teme_selector.addEventListener("click", (event) => {
        var teme = getCookie("tema");

        if(teme == "dark"){
            whiteMode();
        } else {
            darkMode();
        }
    });
}


window.addEventListener("load", (event) => {
    init();
});
  